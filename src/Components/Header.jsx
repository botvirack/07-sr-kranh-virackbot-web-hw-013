import React, { Component } from "react";
import { Nav, Container,Navbar, Form, FormControl, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

export default class Header extends Component {
  render() {
    return (
      <div>
        <Navbar className="bg-light navbar-expand-lg">
          <Container>
          <Navbar.Brand as={Link} to="/">
            AMS
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/">
                Home
              </Nav.Link>
            </Nav>
            <Form inline>
              <FormControl
                 type="text"
                  placeholder="Search"
                  className="mr-sm-2"
              
              />
              <Button variant="outline-primary">Search</Button>
            </Form>
          </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>
    );
  }
}
